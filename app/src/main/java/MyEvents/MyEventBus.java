package MyEvents;

import android.graphics.Bitmap;
import android.view.View;

import com.microsoft.projectoxford.face.contract.Blur;
import com.microsoft.projectoxford.face.contract.Exposure;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.Glasses;
import com.microsoft.projectoxford.face.contract.Noise;

import java.util.List;

public class MyEventBus {

    public static class SendBitmap {

        private Bitmap image1;
        private Bitmap getImage2;

        public SendBitmap(Bitmap image1, Bitmap getImage2) {
            this.image1 = image1;
            this.getImage2 = getImage2;
        }

        public Bitmap getImage1() {
            return image1;
        }

        public void setImage1(Bitmap image1) {
            this.image1 = image1;
        }

        public Bitmap getGetImage2() {
            return getImage2;
        }

        public void setGetImage2(Bitmap getImage2) {
            this.getImage2 = getImage2;
        }
    }

    public static class SendFaces {
        private Face face1;
        private Face face2;

        public SendFaces(Face face1, Face face2) {
            this.face1 = face1;
            this.face2 = face2;
        }

        public Face getFace1() {
            return face1;
        }

        public void setFace1(Face face1) {
            this.face1 = face1;
        }

        public Face getFace2() {
            return face2;
        }

        public void setFace2(Face face2) {
            this.face2 = face2;
        }
    }

    public static class SendBitmapList {

        private List<Bitmap> listBitmap;

        public SendBitmapList(List<Bitmap> listBitmap) {
            this.listBitmap = listBitmap;
        }

        public List<Bitmap> getListBitmap() {
            return listBitmap;
        }

        public void setListBitmap(List<Bitmap> listBitmap) {
            this.listBitmap = listBitmap;
        }
    }

    public static class MyFacesAttribute {
        public double age;
        private String gender;
        private String hair;
        private String facialHair;
        private String makeUp;
        private String emotion;
        private boolean foreheadOccluded;
        private Blur.BlurLevel blurLevel;
        private boolean eyeOccluded;
        private Exposure.ExposureLevel exposureLevel;
        private boolean mouthOccluded;
        private Noise.NoiseLevel noiseLevel;
        private Glasses glasses;
        private String headPose;
        private String accessories;
    }

    public static class SendFacesAttribute {
        private MyFacesAttribute facesAttribute1;
        private MyFacesAttribute facesAttribute2;

        public SendFacesAttribute(MyFacesAttribute facesAttribute1, MyFacesAttribute facesAttribute2) {
            this.facesAttribute1 = facesAttribute1;
            this.facesAttribute2 = facesAttribute2;
        }

        public MyFacesAttribute getFacesAttribute1() {
            return facesAttribute1;
        }

        public void setFacesAttribute1(MyFacesAttribute facesAttribute1) {
            this.facesAttribute1 = facesAttribute1;
        }

        public MyFacesAttribute getFacesAttribute2() {
            return facesAttribute2;
        }

        public void setFacesAttribute2(MyFacesAttribute facesAttribute2) {
            this.facesAttribute2 = facesAttribute2;
        }
    }
    public static class SendView {

        private double age;
        private String gender;
        private String hair;
        private String facialHair;
        private String makeUp;
        private String emotion;
        private boolean foreheadOccluded;
        private Blur.BlurLevel blurLevel;
        private boolean eyeOccluded;
        private Exposure.ExposureLevel exposureLevel;
        private boolean mouthOccluded;
        private Noise.NoiseLevel noiseLevel;
        private Glasses glasses;
        private String headPose;
        private String accessories;

        public SendView(double age, String gender, String hair, String facialHair, String makeUp, String emotion, boolean foreheadOccluded, Blur.BlurLevel blurLevel, boolean eyeOccluded, Exposure.ExposureLevel exposureLevel, boolean mouthOccluded, Noise.NoiseLevel noiseLevel, Glasses glasses, String headPose, String accessories) {
            this.age = age;
            this.gender = gender;
            this.hair = hair;
            this.facialHair = facialHair;
            this.makeUp = makeUp;
            this.emotion = emotion;
            this.foreheadOccluded = foreheadOccluded;
            this.blurLevel = blurLevel;
            this.eyeOccluded = eyeOccluded;
            this.exposureLevel = exposureLevel;
            this.mouthOccluded = mouthOccluded;
            this.noiseLevel = noiseLevel;
            this.glasses = glasses;
            this.headPose = headPose;
            this.accessories = accessories;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getHair() {
            return hair;
        }

        public void setHair(String hair) {
            this.hair = hair;
        }

        public String getFacialHair() {
            return facialHair;
        }

        public void setFacialHair(String facialHair) {
            this.facialHair = facialHair;
        }

        public String getMakeUp() {
            return makeUp;
        }

        public void setMakeUp(String makeUp) {
            this.makeUp = makeUp;
        }

        public String getEmotion() {
            return emotion;
        }

        public void setEmotion(String emotion) {
            this.emotion = emotion;
        }

        public double getAge() {
            return age;
        }

        public void setAge(double age) {
            this.age = age;
        }

        public boolean isForeheadOccluded() {
            return foreheadOccluded;
        }

        public void setForeheadOccluded(boolean foreheadOccluded) {
            this.foreheadOccluded = foreheadOccluded;
        }

        public Blur.BlurLevel getBlurLevel() {
            return blurLevel;
        }

        public void setBlurLevel(Blur.BlurLevel blurLevel) {
            this.blurLevel = blurLevel;
        }

        public boolean isEyeOccluded() {
            return eyeOccluded;
        }

        public void setEyeOccluded(boolean eyeOccluded) {
            this.eyeOccluded = eyeOccluded;
        }

        public Exposure.ExposureLevel getExposureLevel() {
            return exposureLevel;
        }

        public void setExposureLevel(Exposure.ExposureLevel exposureLevel) {
            this.exposureLevel = exposureLevel;
        }

        public boolean isMouthOccluded() {
            return mouthOccluded;
        }

        public void setMouthOccluded(boolean mouthOccluded) {
            this.mouthOccluded = mouthOccluded;
        }

        public Noise.NoiseLevel getNoiseLevel() {
            return noiseLevel;
        }

        public void setNoiseLevel(Noise.NoiseLevel noiseLevel) {
            this.noiseLevel = noiseLevel;
        }

        public Glasses getGlasses() {
            return glasses;
        }

        public void setGlasses(Glasses glasses) {
            this.glasses = glasses;
        }

        public String getHeadPose() {
            return headPose;
        }

        public void setHeadPose(String headPose) {
            this.headPose = headPose;
        }

        public String getAccessories() {
            return accessories;
        }

        public void setAccessories(String accessories) {
            this.accessories = accessories;
        }
    }
}
