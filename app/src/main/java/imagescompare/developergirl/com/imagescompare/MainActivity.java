package imagescompare.developergirl.com.imagescompare;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Accessory;
import com.microsoft.projectoxford.face.contract.Emotion;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FacialHair;
import com.microsoft.projectoxford.face.contract.Hair;
import com.microsoft.projectoxford.face.contract.HeadPose;
import com.microsoft.projectoxford.face.contract.Makeup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import MyEvents.GlobalBus;
import MyEvents.MyEventBus;
import Utility.ImageHelper;

public class MainActivity extends AppCompatActivity {

    Bitmap mBitmap1, mBitmap2;
    ImageView imageView1, imageView2;
    boolean isFirstImage = false;

    ByteArrayInputStream[] lstInputStream;
    List<ByteArrayInputStream> lstbyteArrayStream;

    List<Face[]> faceList;

    List<Face> mReturnFaces;
    List<Bitmap> mReturnfaceThumbnails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
    }

    public void btnCompare(View view) {

        lstInputStream = new ByteArrayInputStream[0];
        lstbyteArrayStream = new ArrayList<>();

        ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
        mBitmap1.compress(Bitmap.CompressFormat.JPEG, 80, outputStream1);
        ByteArrayInputStream inputStream1 = new ByteArrayInputStream(outputStream1.toByteArray());

        ByteArrayOutputStream outputStream2 = new ByteArrayOutputStream();
        mBitmap2.compress(Bitmap.CompressFormat.JPEG, 80, outputStream2);
        ByteArrayInputStream inputStream2 = new ByteArrayInputStream(outputStream2.toByteArray());


        new DetectionTask(MainActivity.this).execute(inputStream1, inputStream2);
    }

    public void selectImage(View view) {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Take Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    if (isFirstImage == true) {
                        mBitmap1 = BitmapFactory.decodeFile(f.getAbsolutePath(),
                                bitmapOptions);
                        imageView1.setImageBitmap(mBitmap1);
                        isFirstImage = false;
                    } else {
                        mBitmap2 = BitmapFactory.decodeFile(f.getAbsolutePath());
                        imageView2.setImageBitmap(mBitmap2);
                        isFirstImage = true;
                    }
                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {
                        outFile = new FileOutputStream(file);
                        if (isFirstImage) {
                            mBitmap1.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        } else {
                            mBitmap2.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        }
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {

                }
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();

                if (isFirstImage == true) {
                    mBitmap1 = BitmapFactory.decodeFile(picturePath);
                    imageView1.setImageBitmap(mBitmap1);
                    isFirstImage = false;
                } else {
                    mBitmap2 = BitmapFactory.decodeFile(picturePath);
                    imageView2.setImageBitmap(mBitmap2);
                    isFirstImage = true;
                }
            }
            FaceListAdapter faceListAdapter = new FaceListAdapter(null);
            Intent intent = new Intent(this, ComparedImagesActiviy.class);
        }
    }

    public class FaceListAdapter extends BaseAdapter {

        List<Face> faces;
        List<Bitmap> faceThumbnails;
        List<MyEventBus.MyFacesAttribute> listMyFacesAttribute=new ArrayList<>();
        MyEventBus.MyFacesAttribute myFacesAttribute=new MyEventBus.MyFacesAttribute();

        FaceListAdapter(Face[] detectionResult) {
            faces = new ArrayList<>();
            faceThumbnails = new ArrayList<>();

            if (detectionResult != null) {
                faces = Arrays.asList(detectionResult);

                for (Face face : faces) {
                    try {
                        faceThumbnails.add(ImageHelper.generateFaceThumbnail(mBitmap1, face.faceRectangle));


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //img2.setImageBitmap(faceThumbnails.get(position));

            // Show the face details.
            DecimalFormat formatter = new DecimalFormat("#0.0");
            String face_description = String.format("Age: %s  Gender: %s\nHair: %s  FacialHair: %s\nMakeup: %s  %s\nForeheadOccluded: %s  Blur: %s\nEyeOccluded: %s  %s\n" +
                            "MouthOccluded: %s  Noise: %s\nGlassesType: %s\nHeadPose: %s\nAccessories: %s",
                    faces.get(position).faceAttributes.age,
                    faces.get(position).faceAttributes.gender,
                    getHair(faces.get(position).faceAttributes.hair),
                    getFacialHair(faces.get(position).faceAttributes.facialHair),
                    getMakeup((faces.get(position)).faceAttributes.makeup),
                    getEmotion(faces.get(position).faceAttributes.emotion),
                    faces.get(position).faceAttributes.occlusion.foreheadOccluded,
                    faces.get(position).faceAttributes.blur.blurLevel,
                    faces.get(position).faceAttributes.occlusion.eyeOccluded,
                    faces.get(position).faceAttributes.exposure.exposureLevel,
                    faces.get(position).faceAttributes.occlusion.mouthOccluded,
                    faces.get(position).faceAttributes.noise.noiseLevel,
                    faces.get(position).faceAttributes.glasses,
                    getHeadPose(faces.get(position).faceAttributes.headPose),
                    getAccessories(faces.get(position).faceAttributes.accessories)
            );

            GlobalBus.getBus().postSticky(new MyEventBus.SendView(faces.get(position).faceAttributes.age,
                    faces.get(position).faceAttributes.gender,
                    getHair(faces.get(position).faceAttributes.hair),
                    getFacialHair(faces.get(position).faceAttributes.facialHair),
                    getMakeup((faces.get(position)).faceAttributes.makeup),
                    getEmotion(faces.get(position).faceAttributes.emotion),
                    faces.get(position).faceAttributes.occlusion.foreheadOccluded,
                    faces.get(position).faceAttributes.blur.blurLevel,
                    faces.get(position).faceAttributes.occlusion.eyeOccluded,
                    faces.get(position).faceAttributes.exposure.exposureLevel,
                    faces.get(position).faceAttributes.occlusion.mouthOccluded,
                    faces.get(position).faceAttributes.noise.noiseLevel,
                    faces.get(position).faceAttributes.glasses,
                    getHeadPose(faces.get(position).faceAttributes.headPose),
                    getAccessories(faces.get(position).faceAttributes.accessories))
              );
            return convertView;
        }

        private String getHair(Hair hair) {
            if (hair.hairColor.length == 0) {
                if (hair.invisible)
                    return "Invisible";
                else
                    return "Bald";
            } else {
                int maxConfidenceIndex = 0;
                double maxConfidence = 0.0;

                for (int i = 0; i < hair.hairColor.length; ++i) {
                    if (hair.hairColor[i].confidence > maxConfidence) {
                        maxConfidence = hair.hairColor[i].confidence;
                        maxConfidenceIndex = i;
                    }
                }

                return hair.hairColor[maxConfidenceIndex].color.toString();
            }
        }

        private String getMakeup(Makeup makeup) {
            return (makeup.eyeMakeup || makeup.lipMakeup) ? "Yes" : "No";
        }

        private String getAccessories(Accessory[] accessories) {
            if (accessories.length == 0) {
                return "NoAccessories";
            } else {
                String[] accessoriesList = new String[accessories.length];
                for (int i = 0; i < accessories.length; ++i) {
                    accessoriesList[i] = accessories[i].type.toString();
                }

                return TextUtils.join(",", accessoriesList);
            }
        }

        private String getFacialHair(FacialHair facialHair) {
            return (facialHair.moustache + facialHair.beard + facialHair.sideburns > 0) ? "Yes" : "No";
        }

        private String getEmotion(Emotion emotion) {
            String emotionType = "";
            double emotionValue = 0.0;
            if (emotion.anger > emotionValue) {
                emotionValue = emotion.anger;
                emotionType = "Anger";
            }
            if (emotion.contempt > emotionValue) {
                emotionValue = emotion.contempt;
                emotionType = "Contempt";
            }
            if (emotion.disgust > emotionValue) {
                emotionValue = emotion.disgust;
                emotionType = "Disgust";
            }
            if (emotion.fear > emotionValue) {
                emotionValue = emotion.fear;
                emotionType = "Fear";
            }
            if (emotion.happiness > emotionValue) {
                emotionValue = emotion.happiness;
                emotionType = "Happiness";
            }
            if (emotion.neutral > emotionValue) {
                emotionValue = emotion.neutral;
                emotionType = "Neutral";
            }
            if (emotion.sadness > emotionValue) {
                emotionValue = emotion.sadness;
                emotionType = "Sadness";
            }
            if (emotion.surprise > emotionValue) {
                emotionValue = emotion.surprise;
                emotionType = "Surprise";
            }
            return String.format("%s: %f", emotionType, emotionValue);
        }

        private String getHeadPose(HeadPose headPose) {
            return String.format("Pitch: %s, Roll: %s, Yaw: %s", headPose.pitch, headPose.roll, headPose.yaw);
        }
    }

    private class DetectionTask extends AsyncTask<InputStream, String, List<Face[]>> {

        private boolean mSucceed = true;

        private final String apiEndpoint = "https://southeastasia.api.cognitive.microsoft.com/face/v1.0";
        private final String subscriptionKey = "7bd4bdd07ae641cebd347e932a11ec85";

        private ProgressDialog dialog;

        public DetectionTask(MainActivity activity) {
            dialog = new ProgressDialog(activity, R.style.full_screen_dialog);
        }

        @Override
        protected List<Face[]> doInBackground(InputStream... inputStreams) {

            FaceServiceClient faceServiceClient = new FaceServiceRestClient(apiEndpoint, subscriptionKey);
            faceList = new ArrayList<>();

            try {
                publishProgress("Detecting");

                Thread.sleep(5000);

                for (int i = 0; i < inputStreams.length; i++) {
                    faceList.add(faceServiceClient.detect(inputStreams[i], true, true, new FaceServiceClient.FaceAttributeType[]{
                            FaceServiceClient.FaceAttributeType.Age,
                            FaceServiceClient.FaceAttributeType.Gender,
                            FaceServiceClient.FaceAttributeType.Smile,
                            FaceServiceClient.FaceAttributeType.Glasses,
                            FaceServiceClient.FaceAttributeType.FacialHair,
                            FaceServiceClient.FaceAttributeType.Emotion,
                            FaceServiceClient.FaceAttributeType.HeadPose,
                            FaceServiceClient.FaceAttributeType.Accessories,
                            FaceServiceClient.FaceAttributeType.Blur,
                            FaceServiceClient.FaceAttributeType.Exposure,
                            FaceServiceClient.FaceAttributeType.Hair,
                            FaceServiceClient.FaceAttributeType.Makeup,
                            FaceServiceClient.FaceAttributeType.Noise,
                            FaceServiceClient.FaceAttributeType.Occlusion
                    }));
                }
                return faceList;

            } catch (Exception e) {
                mSucceed = true;
                publishProgress(e.getMessage());
                return null;
            }


        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Calculating, please wait.");

            dialog.show();


        }

        @Override
        protected void onPostExecute(List<Face[]> faces) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            setUiAfterDetection(faces, mSucceed);
        }
    }

    private void setUiAfterDetection(List<Face[]> faces, boolean mSucceed) {

        if (mSucceed) {
            String detectionFaces;

            if (faces != null) {

                try {
                    Intent intent = new Intent(this, ComparedImagesActiviy.class);

                    //WE SEND TO FACES IN HERE
                    mReturnFaces = new ArrayList<>();
                    mReturnfaceThumbnails = new ArrayList<>();

                    for (Face[] myFaceList : faces) {
                        for (Face myFace : myFaceList) {
                            mReturnFaces.add(myFace);
                        }
                    }
                    GlobalBus.getBus().postSticky(new MyEventBus.SendFaces(mReturnFaces.get(0), mReturnFaces.get(1)));

                    //WE SEND TO BITMAPS IN HERE
                    mReturnfaceThumbnails.add(ImageHelper.generateFaceThumbnail(mBitmap1, mReturnFaces.get(0).faceRectangle));
                    mReturnfaceThumbnails.add(ImageHelper.generateFaceThumbnail(mBitmap2, mReturnFaces.get(1).faceRectangle));

                    GlobalBus.getBus().postSticky(new MyEventBus.SendBitmapList(mReturnfaceThumbnails));

                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
