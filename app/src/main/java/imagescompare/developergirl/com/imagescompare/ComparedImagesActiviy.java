package imagescompare.developergirl.com.imagescompare;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.projectoxford.face.contract.Accessory;
import com.microsoft.projectoxford.face.contract.Emotion;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FacialHair;
import com.microsoft.projectoxford.face.contract.Hair;
import com.microsoft.projectoxford.face.contract.HeadPose;
import com.microsoft.projectoxford.face.contract.Makeup;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import MyEvents.GlobalBus;
import MyEvents.MyEventBus;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComparedImagesActiviy extends AppCompatActivity {

    ImageView imgOnlyFace1, imgOnlyFace2;
    TextView txtAge, txtEmotion, txtPercent;

    @BindView(R.id.txtAccessories) TextView txtAcces;
    @BindView(R.id.txtAccessories2) TextView txtAcces2;
    @BindView(R.id.txtAge2) TextView txtAge2;
    @BindView(R.id.txtEmotion2) TextView txtEmotion2;
    @BindView(R.id.txtBlurLEvel) TextView txtBlurLevel;
    @BindView(R.id.txtBlurLEvel2) TextView txtBlurLevel2;
    @BindView(R.id.txtEyeOcc) TextView txtEyeOcc;
    @BindView(R.id.txtEyeOcc2) TextView txtEyeOcc2;
    @BindView(R.id.txtFacialHair) TextView txtFacialHair;
    @BindView(R.id.txtFacialHair2) TextView txtFacialHair2;
    @BindView(R.id.txtForHeadOcc) TextView txtForHeadOcc;
    @BindView(R.id.txtForHeadOcc2) TextView txtForHeadOcc2;
    @BindView(R.id.txtGender) TextView txtGender;
    @BindView(R.id.txtGender2) TextView txtGender2;
    @BindView(R.id.txtGlasses) TextView txtGlasses;
    @BindView(R.id.txtGlasses2) TextView txtGlasses2;
    @BindView(R.id.txtHair) TextView txtHair;
    @BindView(R.id.txtHair2) TextView txtHair2;
    @BindView(R.id.txtHeadPose) TextView txtHeadPose;
    @BindView(R.id.txtHeadPose2) TextView txtHeadPose2;
    @BindView(R.id.txtMakeUp) TextView txtMakeUp;
    @BindView(R.id.txtMakeUp2) TextView txtMakeUp2;
    @BindView(R.id.txtMoutOcc) TextView txtMouthOcc;
    @BindView(R.id.txtMoutOcc2) TextView txtMouthOcc2;
    @BindView(R.id.txtNoiseLevel) TextView txtNoiseLevel;
    @BindView(R.id.txtNoiseLevel2) TextView txtNoiseLevel2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_compared_images);
        ButterKnife.bind(this);

        imgOnlyFace1 = findViewById(R.id.imgOnlyFace1);
        imgOnlyFace2 = findViewById(R.id.imgOnlyFace2);
        txtAge = findViewById(R.id.txtAge);
        txtEmotion = findViewById(R.id.txtEmotion);
        txtPercent = findViewById(R.id.txtPercent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        GlobalBus.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        GlobalBus.getBus().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventGetView(MyEventBus.SendView view) {

    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEventGetFaces(MyEventBus.SendFaces listFaces) {
        Face face1 = listFaces.getFace1();
        Face face2 = listFaces.getFace2();

        txtEmotion.setText(getEmotion(face1.faceAttributes.emotion));
        txtAcces.setText(getAccessories(face1.faceAttributes.accessories));
        txtHair.setText(getHair(face1.faceAttributes.hair));
        txtMakeUp.setText(getMakeup(face1.faceAttributes.makeup));
        txtFacialHair.setText(getFacialHair(face1.faceAttributes.facialHair));
        txtHeadPose.setText(getHeadPose(face1.faceAttributes.headPose));
        txtAge.setText(String.valueOf(face1.faceAttributes.age));
        txtBlurLevel.setText(String.valueOf(face1.faceAttributes.blur));
        txtEyeOcc.setText(String.valueOf(face1.faceAttributes.occlusion));
        txtGender.setText(String.valueOf(face1.faceAttributes.gender));
        txtGlasses.setText(String.valueOf(face1.faceAttributes.glasses));
        txtNoiseLevel.setText(String.valueOf(face1.faceAttributes.noise));

        txtEmotion2.setText(getEmotion(face2.faceAttributes.emotion));
        txtAcces2.setText(getAccessories(face2.faceAttributes.accessories));
        txtHair2.setText(getHair(face2.faceAttributes.hair));
        txtMakeUp2.setText(getMakeup(face2.faceAttributes.makeup));
        txtFacialHair2.setText(getFacialHair(face2.faceAttributes.facialHair));
        txtHeadPose2.setText(getHeadPose(face2.faceAttributes.headPose));
        txtAge2.setText(String.valueOf(face2.faceAttributes.age));
        txtBlurLevel2.setText(String.valueOf(face2.faceAttributes.blur));
        txtEyeOcc2.setText(String.valueOf(face2.faceAttributes.occlusion));
        txtGender2.setText(String.valueOf(face2.faceAttributes.gender));
        txtGlasses2.setText(String.valueOf(face2.faceAttributes.glasses));
        txtNoiseLevel2.setText(String.valueOf(face2.faceAttributes.noise));

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvenGetBitmaps(MyEventBus.SendBitmapList listBitmaps) {
        double sonuc;

        Bitmap bitmap1 = listBitmaps.getListBitmap().get(0);
        Bitmap bitmap2 = listBitmaps.getListBitmap().get(1);

        Bitmap resizedBitmap1 = Bitmap.createScaledBitmap(
                bitmap1, 120, 120, false);
        Bitmap resizedBitmap2 = Bitmap.createScaledBitmap(
                bitmap2, 120, 120, false);

        imgOnlyFace1.setImageBitmap(resizedBitmap1);
        imgOnlyFace2.setImageBitmap(resizedBitmap2);

        double diff = 0;

   /*     long calculateWidth, calculateHeigth;

        if (resizedBitmap1.getWidth() > resizedBitmap2.getWidth()) {
            calculateWidth = resizedBitmap2.getWidth();
        } else {
            calculateWidth = resizedBitmap1.getWidth();
        }
        if (resizedBitmap1.getHeight() > resizedBitmap2.getHeight()) {
            calculateHeigth = resizedBitmap2.getHeight();
        } else {
            calculateHeigth = resizedBitmap1.getHeight();
        }*/

        int top1 = 0, top2;
        boolean b = resizedBitmap1.sameAs(resizedBitmap2);

        for (int i = 0; i < resizedBitmap1.getWidth(); i++) {
            for (int j = 0; j < resizedBitmap1.getHeight(); j++) {

                int pixel = resizedBitmap1.getPixel(i, j);
                int redValue = Color.red(pixel);
                int blueValue = Color.blue(pixel);
                int greenValue = Color.green(pixel);

                // yüz rengi r 200 - 250 g 150-200 b 50 -100

                int pixel2 = resizedBitmap2.getPixel(i, j);
                int redValue2 = Color.red(pixel2);
                int blueValue2 = Color.blue(pixel2);
                int greenValue2 = Color.green(pixel2);

                diff += (float) Math.abs(redValue - redValue2) / 255;
                diff += (float) Math.abs(blueValue - blueValue2) / 255;
                diff += (float) Math.abs(greenValue - greenValue2) / 255;

                Log.e("hesaplama", "i=" + i + "  " + "j=" + j);
            }
        }

        sonuc = 100 * diff / (120 * 120 * 3);

        if (sonuc < 11) {

        }

        txtPercent.setText("Farklılık % " + sonuc);
    }

    private String getEmotion(Emotion emotion) {
        String emotionType = "";
        double emotionValue = 0.0;
        if (emotion.anger > emotionValue) {
            emotionValue = emotion.anger;
            emotionType = "Anger";
        }
        if (emotion.contempt > emotionValue) {
            emotionValue = emotion.contempt;
            emotionType = "Contempt";
        }
        if (emotion.disgust > emotionValue) {
            emotionValue = emotion.disgust;
            emotionType = "Disgust";
        }
        if (emotion.fear > emotionValue) {
            emotionValue = emotion.fear;
            emotionType = "Fear";
        }
        if (emotion.happiness > emotionValue) {
            emotionValue = emotion.happiness;
            emotionType = "Happiness";
        }
        if (emotion.neutral > emotionValue) {
            emotionValue = emotion.neutral;
            emotionType = "Neutral";
        }
        if (emotion.sadness > emotionValue) {
            emotionValue = emotion.sadness;
            emotionType = "Sadness";
        }
        if (emotion.surprise > emotionValue) {
            emotionValue = emotion.surprise;
            emotionType = "Surprise";
        }
        return String.format("%s: %f", emotionType, emotionValue);
    }

    private String getHair(Hair hair) {
        if (hair.hairColor.length == 0) {
            if (hair.invisible)
                return "Invisible";
            else
                return "Bald";
        } else {
            int maxConfidenceIndex = 0;
            double maxConfidence = 0.0;

            for (int i = 0; i < hair.hairColor.length; ++i) {
                if (hair.hairColor[i].confidence > maxConfidence) {
                    maxConfidence = hair.hairColor[i].confidence;
                    maxConfidenceIndex = i;
                }
            }

            return hair.hairColor[maxConfidenceIndex].color.toString();
        }
    }

    private String getMakeup(Makeup makeup) {
        return (makeup.eyeMakeup || makeup.lipMakeup) ? "Yes" : "No";
    }

    private String getAccessories(Accessory[] accessories) {
        if (accessories.length == 0) {
            return "NoAccessories";
        } else {
            String[] accessoriesList = new String[accessories.length];
            for (int i = 0; i < accessories.length; ++i) {
                accessoriesList[i] = accessories[i].type.toString();
            }

            return TextUtils.join(",", accessoriesList);
        }
    }

    private String getFacialHair(FacialHair facialHair) {
        return (facialHair.moustache + facialHair.beard + facialHair.sideburns > 0) ? "Yes" : "No";
    }

    private String getHeadPose(HeadPose headPose) {
        return String.format("Pitch: %s, Roll: %s, Yaw: %s", headPose.pitch, headPose.roll, headPose.yaw);
    }

}
